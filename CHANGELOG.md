# Changelog for Fluent Opening Hours

All notable changes to `fluent-opening-hours` will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.1.2] - 2020-03-25
### Fixed
- Method signature extended to take in count the options

## [1.1.1] - 2020-03-25
### Added
- Unit tests for the Options class

### Fixed
- PHPUnit configuration for whitelisting

## [1.1.0] - 2020-03-25
### Added
- Magic method for static calls to `parse` method

## [1.0.0] - 2020-03-24
### Added
- Working codebase for formatting opening hours
