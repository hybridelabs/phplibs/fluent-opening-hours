<?php
declare(strict_types=1);

namespace HybrideLabs\FluentOpeningHours;

/**
 * Default strings
 * @package HybrideLabs\FluentOpeningHours
 */
class Options
{

    public string $separator = ":";

    public string $from = "from";

    public string $to = "to";

    public string $and = "and";

    public string $closed = "closed";

    public string $noDataMessage = "No opening data available";

    public array $days = [
        0 => 'monday',
        1 => 'tuesday',
        2 => 'wednesday',
        3 => 'thursday',
        4 => 'friday',
        5 => 'saturday',
        6 => 'sunday',
    ];

    /**
     * Options constructor.
     *
     * @param  array|null  $options
     */
    public function __construct(?array $options = null)
    {
        if (!is_null($options)) {
            /**
             * @var string $key
             * @var string|array $option
             */
            foreach ($options as $key => $option) {
                $this->$key = $option;
            }
        }
    }

    public static function set(?array $options = null): self
    {
        return new Options($options);
    }
}
