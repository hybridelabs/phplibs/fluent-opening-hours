<?php
declare(strict_types=1);

namespace HybrideLabs\FluentOpeningHours;

use BadMethodCallException;
use HybrideLabs\FluentOpeningHours\Exceptions\ExcessiveDaysException;
use HybrideLabs\FluentOpeningHours\Exceptions\InsufficientDaysException;
use InvalidArgumentException;

/**
 * Class FluentOpeningHours
 * @package HybrideLabs\FluentOpeningHours
 *
 * @method static parse(array $openingHoursArray, ?Options $options = null)
 */
class FluentOpeningHours
{

    /** @var bool $openOnlyFlag */
    private bool $openOnlyFlag = false;

    /** @var bool $hasOpeningHours */
    private bool $hasOpeningHours = false;

    /** @var Options */
    private Options $options;

    /** @var array $submittedOpeningHours */
    private array $openingHours = [];

    /** @var array $formattedOpeningHours */
    private array $formattedOpeningHours = [];


    /**
     * FluentOpeningHours constructor.
     *
     * @param  Options  $options
     */
    public function __construct(?Options $options = null)
    {
        if (is_null($options)) {
            $options = new Options();
        }
        $this->options = $options;
    }

    /**
     * Allows the static call to the internal method for parsing the array
     *
     * @param $name
     * @param $arguments
     *
     * @return FluentOpeningHours
     * @throws InvalidArgumentException
     * @throws BadMethodCallException
     */
    public static function __callStatic(string $name, array $arguments = []): FluentOpeningHours
    {
        if (empty($arguments)) {
            throw new InvalidArgumentException;
        }
        if (isset($arguments[1])) {
            if (!$arguments[1] instanceof Options) {
                throw new InvalidArgumentException;
            }
            /** @var Options $options */
            $options = $arguments[1];
        } else {
            $options = new Options;
        }
        $instance = new FluentOpeningHours($options);
        if ($name == "parse") {
            /** @var FluentOpeningHours $return */
            $return = call_user_func(array($instance, "parseArray"), (array) $arguments[0]);

            return $return;
        }
        throw new BadMethodCallException;
    }

    /**
     * Allows the usage of the same name as the static version for parsing
     *
     * @param  string  $name
     * @param  array  $arguments
     *
     * @return FluentOpeningHours
     * @throws InvalidArgumentException
     * @throws BadMethodCallException
     */
    public function __call(string $name, array $arguments = []): FluentOpeningHours
    {
        if (empty($arguments)) {
            throw new InvalidArgumentException;
        }
        if ($name == "parse") {
            /** @var FluentOpeningHours $return */
            $return = call_user_func(array($this, "parseArray"), $arguments[0]);

            return $return;
        }
        throw new BadMethodCallException;
    }

    /**
     * Setter for $openOnlyFlag
     *
     * @return FluentOpeningHours
     */
    public function openOnly(): FluentOpeningHours
    {
        $this->openOnlyFlag = true;

        return $this;
    }

    /**
     * @return string
     */
    public function format(): string
    {
        if (!$this->hasOpeningHours) {
            return $this->options->noDataMessage;
        }
        array_map(function ($index, $string) {
            $this->formattedOpeningHours[(string) $this->options->days[$index]] = $string;
        }, array_keys($this->openingHours), $this->openingHours);

        $rangeGroups = [];
        /**
         * @var string $day
         * @var string $range
         */
        foreach ($this->formattedOpeningHours as $day => $range) {
            if ($this->openOnlyFlag) {
                if ($range == $this->options->closed) {
                    unset($this->formattedOpeningHours[$day]);
                    continue;
                }
            }

            $hash                 = md5((string) $range);
            $rangeGroups[$hash][] = $day;
        }

        if (!empty($rangeGroups)) {
            foreach ($rangeGroups as $group => $days) {
                $count    = count($days);
                $dayGroup = "";
                $lastDay  = "";
                $index    = 1;
                foreach ($days as $key => $day) {
                    if ($index != $count) {
                        $dayGroup .= ", ".$day;
                        unset($this->formattedOpeningHours[$day]);
                    } else {
                        if ($count != 1) {
                            $dayGroup .= " ".$this->options->and." ".$day;
                        } else {
                            $dayGroup .= ", ".$day;
                        }
                        $lastDay = $day;
                    }
                    $index++;
                }
                $this->formattedOpeningHours[$lastDay] = ltrim($dayGroup,
                        ", ")." ".(string) $this->formattedOpeningHours[$lastDay].", ";
            }
        }

        return rtrim(implode($this->formattedOpeningHours), ", ");
    }

    /**
     * @param  array|null  $openingHours
     *
     * @return FluentOpeningHours
     * @throws ExcessiveDaysException
     * @throws InsufficientDaysException
     */
    private function parseArray(?array $openingHours = null): FluentOpeningHours
    {
        if (is_null($openingHours)) {
            throw new InvalidArgumentException;
        }
        $this->check(count($openingHours));

        /**
         * @var int $key
         * @var array|mixed $daily
         */
        foreach ($openingHours as $key => $daily) {
            if (!is_array($daily)) {
                throw new InvalidArgumentException;
            }
            if (!empty($daily)) {
                $this->hasOpeningHours = true;
                break;
            }
        }
        for ($index = 0; $index <= 6; $index++) {
            $range                      = (array) $openingHours[$index];
            $this->openingHours[$index] = $this->build($range);
        }

        return $this;
    }

    /**
     * @param  int  $count
     *
     * @throws ExcessiveDaysException
     * @throws InsufficientDaysException
     */
    private function check(int $count): void
    {
        if (($comparison = $count <=> 7) != 0) {
            if ($comparison == -1) {
                throw new InsufficientDaysException;
            }
            if ($comparison == 1) {
                throw new ExcessiveDaysException;
            }
        }
    }

    /**
     * @param  array  $range
     *
     * @return string
     */
    private function build(array $range): string
    {
        $count = count($range);
        if ($count == 0) {
            return $this->options->closed;
        }
        $from = $this->options->from;
        $to   = $this->options->to;

        $string = "";
        for ($index = 0; $index < count($range); $index++) {
            if ($index > 0) {
                $string .= " ".$this->options->and." ";
            }
            $string .= $from." ".str_replace('-', " ".$to." ", (string) $range[$index]);
        }

        return $string;
    }

}
