<?php
declare(strict_types=1);

namespace HybrideLabs\FluentOpeningHours\Exceptions;

use Exception;

class InsufficientDaysException extends Exception
{
}
